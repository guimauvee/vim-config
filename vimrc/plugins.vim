call plug#begin('~/.vim/plugged')


" -----------------------
" --- UI enhancements ---
" -----------------------
" File explorer within Vim
Plug 'scrooloose/nerdtree'
" Ultimate Vim statusline utility
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Highlights the matching HTML tag
Plug 'gregsexton/MatchTag'
" Rainbow parentheses!! :)
Plug 'kien/rainbow_parentheses.vim'


" ----------------
" --- Movement ---
" ----------------
" Easier vim motions. Try <Leader><Leader>w or <Leader><Leader>fo
Plug 'Lokaltog/vim-easymotion'
" Use to fuzzy find file
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
" Smooth scrolling
Plug 'cskeeters/vim-smooth-scroll'


" ------------------------
" --- Vim enhancements ---
" ------------------------
" Allow tab completion when searching
Plug 'vim-scripts/SearchComplete'
" Adds visualizations for vim marks
Plug 'kshenoy/vim-signature'
" Break or join lines
Plug 'AndrewRadev/splitjoin.vim'


" ----------------------------
" --- Editing enhancements ---
" ----------------------------
" Shortcuts to comment code. Use <Leader>cc or <Leader>c<Space>
Plug 'tpope/vim-commentary'
" Simple shortcuts to deal with surrounding symbols
Plug 'tpope/vim-surround'
" Help end certain structures automatically
Plug 'tpope/vim-endwise'

" Syntax checking
Plug 'dense-analysis/ale'
	

" ------------------------
" --- Java development ---
" ------------------------
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" also call once  :CocInstall coc-java

" Code outline
Plug 'majutsushi/tagbar'
" ^ needs ctags, example : sudo apt install universal-ctags


" --------------------------
" --- Python development ---
" --------------------------
" class, function and docstring text object
Plug 'jeetsukumaran/vim-pythonsense'
" Auto generate docstring
Plug 'heavenshell/vim-pydocstring'
" Pytest support
Plug 'vtannguyen/vimux-pytest.vim'
" Code formating. yapf need to install first via `pip install yapf`
Plug 'mindriot101/vim-yapf'


" ------------------------------
" --- Javascript development ---
" ------------------------------
Plug 'maxmellon/vim-jsx-pretty'
Plug 'alvan/vim-closetag'
" Code formatter
Plug 'prettier/vim-prettier', {
  \ 'do': 'yarn install',
  \ 'for': ['javascript', 'typescript', 'css', 'less', 'scss', 'json', 'graphql', 'markdown', 'vue', 'yaml', 'html'] }

" ------------------------
" --- Vim Text Objects ---
" ------------------------
Plug 'wellle/targets.vim'


" -------------------
" --- Git for Vim ---
" -------------------
" Plugin for git
Plug 'tpope/vim-fugitive'
Plug 'Xuyuanp/nerdtree-git-plugin'
" Show git diff
Plug 'airblade/vim-gitgutter'


" ----------------
" ----- Tmux -----
" ----------------
" vim-tmux navigator
Plug 'christoomey/vim-tmux-navigator'
" run tmux command from vim termina
Plug 'benmills/vimux'


" Utilities, Dependencies
Plug 'vim-scripts/L9'


call plug#end()
