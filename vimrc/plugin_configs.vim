" NERD_Tree
map <C-n> :NERDTreeToggle<CR>
let g:NERDTreeWinPos = 'right'
map <Leader>n :NERDTreeFind<cr>
" enable line numbers
let NERDTreeShowLineNumbers=1
" make sure relative line numbers are used
autocmd FileType nerdtree setlocal relativenumber
let g:NERDTreeShowBookmarks = 1
let g:NERDTreeShowHidden = 1
let NERDTreeWinSize = 40
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
"let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let g:NERDTreeGitStatusIndicatorMapCustom = {
    \ "Modified"  : "~",
    \ "Staged"    : "+",
    \ "Untracked" : "*",
    \ "Renamed"   : "»",
    \ "Unmerged"  : "=",
    \ "Deleted"   : "-",
    \ "Dirty"     : "×",
    \ "Clean"     : "ø",
    \ 'Ignored'   : '☒',
    \ "Unknown"   : "?"
    \ }

" Airline theme
let g:airline_theme='base16_atelierforest'

" Rainbow Parentheses
" Always on
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" tagbar
nmap <Leader>t :TagbarToggle<CR>


" Only run linters named in ale_linters settings.
let g:ale_linters_explicit = 1

let g:ale_linters = {
\   'javascript': ['eslint'],
\   'python': ['pylint'],
\}

" pydocstring
let g:pydocstring_templates_dir = '~/.vim/pydocstring_template/'
let g:pydocstring_enable_mapping = 0

" Vim close tag
let g:closetag_filenames = '*.html,*.js,*.phtml'
