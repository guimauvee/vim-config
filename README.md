To use this configuration, clone this repository to your ~/.vim:
```
git clone https://gitlab.com/guimauvee/vim-config.git ~/.vim
```
then move the `.vimrc` to `~/.vimrc`.

Launch vim and call `:PlugInstall` to install all the plugins.


Additional configuration info:

- In order to have Java support (with Gradle and Maven as well), install the
  coc-java plugin by calling in vim:
```:CocInstall coc-java```

- To have the Tagbar (code outline) to work, some version of ctags is needed.
  This can still be added after the plugin has been installed. For debian you
 can install one with:
```sudo apt install universal-ctags```

Many thanks to Tan and [here is his version of
this](https://github.com/vtannguyen/vim-dotfiles/)!
